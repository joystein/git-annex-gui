v0.2.5 - 2021-03-09
-------------------
Add basic documentation to README

- [doc] how to connect to existing repo
- [doc] fix rendering in pypi
- [doc] how to use ssh-agent for pw-less login
- [doc] how to create ssh keys

v0.2 - 2019-03-19
-----------------
Settle on language and delivery. Implement basic features

- [feature] Implement start with `git-annex assistant --autostart`
- [feature] Implement stop with `git-annex assistant --autostop`
- [x] #2 Port c++ part to python.
- [x] #2 Implement start and stop of git annex daemon.
- [x] Find out how to publish/deliver app.
- [x] Publish to pypi and publich gitlab/github.
- [x] Add command helper.
- [x] Remove c++ and CMake files.
- [x] Add more information to setup.py for pypi.

v0.1 - 2019-01-21
-----------------
how far with qml only

- [x] #1 Add icon to systray. Use the git annex icon.
- [x] Open/close the webview window by clicking the systray icon.
